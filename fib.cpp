#include <iostream>
#include <vector>

using namespace std;

unsigned long fibR(unsigned n)//recursive fibonacci implementation
{
  
   if (n <= 1)
	  return n;
  
  else
	  return fibR(n-1) + fibR(n-2);
  
}



unsigned long fibDP(unsigned n)//dynamic programming fibonacci implementation
{
   //This array will store the Fibonacci numbers for future reference
  int f[n+2];
  int i;
  
  f[0] = 0;
  f[1] = 1;
  
  for(i = 2; i <= n; i++)
  {
	  //Adding the previous 2 numbers in the array and then proceed to store it
	  f[i] = f[i-1] + f[i-2];
  }
  
  return f[n];
  
  
  
  
}


int main()
{

  unsigned input;

  
   cerr<<"Welcome to \"Fibonacci Comparison Program\". We first need some input :  ";

    cin>>input;

    cerr<<endl<<"fibDP("<<input<<") =  "<<fibDP(input)<<endl;

    cerr<<endl<<"fibR("<<input<<") =  "<<fibR(input)<<endl;

    return 0;
}
